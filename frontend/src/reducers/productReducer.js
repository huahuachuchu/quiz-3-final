const initial = {
    product: null
};

export const productReducer = (state = initial, action) => {
    switch (action.type) {
        case "GET_ALL_PRODUCTS":
            return {
                ...state,
                product: action.payload
            };
        default:
            return state;
    }
};
