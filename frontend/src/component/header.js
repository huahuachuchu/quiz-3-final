import React, {Component} from 'react';
import { MdEventNote } from "react-icons/md";

class Header extends Component {
  render() {
    return (
      <div className="header">
        <MdEventNote size={'40'}/>
        <p>商城</p>
        <p>订单</p>
        <p>添加商品</p>
      </div>
    );
  }
}

export default Header;
