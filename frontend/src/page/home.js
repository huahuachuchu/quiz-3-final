import React, {Component} from 'react';
import {getAllProducts} from "../action/getAllProducts";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

class Home extends Component {


    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        this.props.getAllProducts();
    }

    render() {
        return (
            <div>
                {this.props.products.map()}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    products: state.productReducer.products
});

const mapDispatchToProps = (dispatch) => ({
    getAllProducts: () => {
        dispatch(getAllProducts())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);


