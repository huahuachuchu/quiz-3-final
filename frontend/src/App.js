import React, {Component} from 'react';
import './App.less';
import Home from "./page/home";
import Header from "./component/header";

class App extends Component {
    render() {
        return (
            <div className='App'>
                <Header/>
            </div>
        );
    }
}

export default App;
