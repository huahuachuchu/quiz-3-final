package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.domain.Post;
import com.twuc.webApp.domain.PostRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class PostControllerTest extends ApiTestBase {
    @Autowired
    private PostRepository repository;

    private List<Long> createPosts(List<Post> posts) {
        List<Long> ids = new ArrayList<>();

        clear(em -> {
            repository.saveAll(posts);
            repository.flush();
            ids.addAll(posts.stream().map(Post::getId).collect(Collectors.toList()));
        });

        return ids;
    }

    @Test
    void should_get_all_posts() throws Exception {
        createPosts(Arrays.asList(new Post("可乐",2,"https://gd1.alicdn.com/imgextra/i1/1942900995/O1CN01qTrF141JDkcnqoUQq_!!1942900995.jpg",3),
                new Post("雪碧",4,"https://gd1.alicdn.com/imgextra/i1/2656379929/O1CN012nsP892NDX3olmZaD_!!2656379929.jpeg",2)));

        String json = getMockMvc().perform(get("/api/posts"))
                .andExpect(status().is(200))
                .andReturn().getResponse().getContentAsString();

        PostResponseForTest[] posts = new ObjectMapper().readValue(json, PostResponseForTest[].class);

        assertEquals(2, posts.length);
        assertEquals("可乐", posts[0].getName());
        assertEquals(2, posts[0].getPrice());
        assertEquals("https://gd1.alicdn.com/imgextra/i1/1942900995/O1CN01qTrF141JDkcnqoUQq_!!1942900995.jpg", posts[0].getImage());
        assertEquals(3,posts[0].getQuantity());
        assertEquals("雪碧", posts[1].getName());
        assertEquals(4, posts[1].getPrice());
        assertEquals("https://gd1.alicdn.com/imgextra/i1/2656379929/O1CN012nsP892NDX3olmZaD_!!2656379929.jpeg", posts[1].getImage());
        assertEquals(2,posts[1].getQuantity());
    }

    @Test
    void should_get_null_posts() throws Exception {
        String json = getMockMvc().perform(get("/api/posts"))
                .andExpect(status().is(200))
                .andReturn().getResponse().getContentAsString();

        PostResponseForTest[] posts = new ObjectMapper().readValue(json, PostResponseForTest[].class);

        assertEquals(0, posts.length);
    }

    @Test
    void should_add_product() throws Exception {
        getMockMvc().perform(post("/api/posts")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductsTest("芬达", 2,"https://gd1.alicdn.com/imgextra/i1/2656379929/O1CN012nsP892NDX3olmZaD_!!2656379929.jpeg",7))))
                .andExpect(status().is(201));

        List<Post> posts = repository.findAll();

        assertEquals(1, posts.size());
        assertEquals("芬达", posts.get(0).getName());
        assertEquals(2, posts.get(0).getPrice());
        assertEquals("https://gd1.alicdn.com/imgextra/i1/2656379929/O1CN012nsP892NDX3olmZaD_!!2656379929.jpeg", posts.get(0).getImage());
        assertEquals(7, posts.get(0).getQuantity());

    }

}

class PostResponseForTest {
    private Long id;
    private String name;
    private int price;
    private String image;
    private int quantity;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public int getQuantity() {
        return quantity;
    }
}

class CreateProductsTest {
    private String name;
    private int price;
    private String image;
    private int quantity;

    public CreateProductsTest(String name, int price, String image, int quantity) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public int getQuantity() {
        return quantity;
    }
}
