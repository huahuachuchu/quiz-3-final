package com.twuc.webApp.web;

import com.twuc.webApp.contracts.GetProduct;
import com.twuc.webApp.contracts.GreatPostRequest;
import com.twuc.webApp.domain.Post;
import com.twuc.webApp.domain.PostRepository;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/posts")
@CrossOrigin(origins = "*")
public class PostController {
    private final  PostRepository postRepository;

    public PostController(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @GetMapping
    public ResponseEntity<List<GetProduct>> getAll() {
        List<GetProduct> posts = postRepository.findAll(
                Sort.by(Sort.Direction.ASC, "id")).stream()
                .map(p -> new GetProduct(p.getId(), p.getName(), p.getPrice(),p.getImage(),p.getQuantity()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(posts);
    }

    @PostMapping
    public ResponseEntity create(@RequestBody @Valid GreatPostRequest request) {
        if (request == null) return ResponseEntity.badRequest().build();
        postRepository.save(new Post(request.getName(), request.getPrice(),request.getImage(),request.getQuantity()));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
