package com.twuc.webApp.contracts;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class GreatPostRequest {
    @NotNull
    @Size(min = 1,max = 128)
    private String name;
    @NotNull
    private int price;
    @NotNull
    @Size(min = 1,max = 128)
    private String image;
    @NotNull
    private int quantity;

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public int getQuantity() {
        return quantity;
    }
}
