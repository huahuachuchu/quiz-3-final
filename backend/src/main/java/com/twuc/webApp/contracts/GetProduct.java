package com.twuc.webApp.contracts;

public class GetProduct {
    private Long id;
    private String name;
    private int price;
    private String image;
    private int quantity;

    public GetProduct() {
    }

    public GetProduct(Long id, String name, int price, String image, int quantity) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public int getQuantity() {
        return quantity;
    }
}
