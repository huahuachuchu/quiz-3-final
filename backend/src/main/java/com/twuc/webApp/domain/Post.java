package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false,columnDefinition = "TEXT")
    private String name;
    @Column(nullable = false)
    private int price;
    @Column(nullable = false)
    private String image;
    @Column(nullable = false)
    private int quantity;

    public Post() {
    }

    public Post(String name, int price, String image, int quantity) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public int getQuantity() {
        return quantity;
    }

}
